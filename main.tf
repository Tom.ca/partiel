terraform {
  backend "gcs" {
    bucket = "terraform-state-partiel-grp2"
  }
}

provider "google" {
  credentials = file("./partiel-393312-e1ee55201342.json")
  project     = "partiel-393312"
  region      = "europe-west1"
}

resource "google_compute_instance" "default" {
  name         = "terraform-instance"
  machine_type = "e2-micro"
  zone         = "europe-west9-a"
  metadata_startup_script = file("./startup-script.sh")
  tags = ["http-server", "https-server", "ssh"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = "default"

    access_config {
      // Ephemeral IP
    }
  }
}
